import { Stats } from "../Classes/Stats";
import { Battle } from "../Classes/battle/Battle";

var battleResults = [
  "1 Hero dealt 70 damage to Enemy",
  "2 Enemy dealt 10 damage to Hero",
  "3 Hero dealt 70 damage to Enemy",
  "4 Hero  has won! Hero has 2 xp",
];

let btnGet = document.querySelector("button");
let myTable = document.querySelector("#table");

const subTitle = "Clout : human vs Wherewolf" as string;

let headers = ["Round"];

btnGet.addEventListener("click", () => {
  let card = document.createElement("div");
  let table = document.createElement("table");
  let headerRow = document.createElement("tr");

  headers.forEach((headerText: any) => {
    let header = document.createElement("th");
    let textNode = document.createTextNode(headerText);
    header.appendChild(textNode);
    headerRow.appendChild(header);
  });

  table.appendChild(headerRow);

  battleResults.forEach((emp) => {
    let row = document.createElement("tr");

    Object.values(emp).forEach((text) => {
      let cell = document.createElement("td");
      let textNode = document.createTextNode(text);
      cell.appendChild(textNode);
      row.appendChild(cell);
    });

    table.appendChild(row);
  });

  document.getElementById("subtitle")!.innerHTML = `${subTitle}`;

  myTable.appendChild(table);
});
