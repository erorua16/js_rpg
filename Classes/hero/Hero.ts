import { Character } from '../Character';
import { HeroRace } from './HeroRace';
export class Hero extends Character {

    //Variables declarations
    private race:HeroRace

    //Constructor
    constructor(name: string, race:HeroRace, health:number){

        super();
        this.name = name;
        this.maxHealth = health;
        this.health = health;
        this.hitStrength = 7;
        this.lvl = 1;
        this.race = race;
        this.canDefend = true;
    };
    
    //RACE
    //To set new race
    setRace(race:HeroRace){
        this.race = race;
    }
    //To get race
    getRace():HeroRace{
        return this.race;
    }

    //ACTIONS
    //To attack
    attack(race:HeroRace, enemyFly:boolean ){
        switch (race) {

            //Race Elf
            case HeroRace.elf:
                //Hit strength bonus +10% if enemy flies
                if (enemyFly == true){
                    return((this.hitStrength * 1.1) * this.lvl)
                    break;
                }
                //Hit strength handicap -10% if enemy doesn't fly
                return((this.hitStrength / 1.1) * this.lvl)
                break;

            //Race Dwarf
            case HeroRace.dwarf:
                return(this.hitStrength * this.lvl)
                break;

            //Race Human
            default:
                //Hit strength bonus +10% if enemy doesn't fly
                if (enemyFly !== true){
                    return((this.hitStrength * 1.1) * this.lvl)
                    break;
                }
                //Hit strength handicap -10% if enemy flies
                return((this.hitStrength / 1.1) * this.lvl)
                break;
                
        }
    }

    //To defend from attack
    defence(race:HeroRace, damage: number){
        switch (race) {
            // 20% chance to get 50% less damage
            case HeroRace.dwarf: 
                const chance:number = 20.00;
                var n = Math.random();
                n = n * 100;
                if(n <= chance)
                {
                    return damage / 2;
                    break;
                }
                return damage;
                break;
            default: 
                return damage;
                break;
        }
    }

    //To gain 2 xp
    gainXP(){
        this.setXp(this.xp + 2) ;
        if(this.xp >= 10){
           return this.lvlUp()
        }
        return ( this.name + ' ' + 'gained 2 xp' + ' ' + '.' + ' ' + this.name + ' ' + 'has' + ' ' + this.xp + ' ' + 'xp total')
    }
    //To level up, level up by 1 once xp reaches 10
    lvlUp(){
        while(this.xp >= 10){
            this.setLvl(this.lvl + 1);
            this.setXp(this.xp - 10)
        }
        return ( this.name + ' ' + 'leveled up! You are now level' + ' ' + this.getLvl())
    }
    //To restore health by 10% of enemy max health
    restoreHealth(enemyHealth:number){
        if (this.health < this.maxHealth){
            this.setHealth(this.health + Math.round(enemyHealth * 0.1));
            if(this.health > this.maxHealth){
                this.setHealth(this.maxHealth)
            }
            return (this.name + ' ' + 'restored' + ' ' + Math.round(enemyHealth * 0.1) + ' ' + 'health points')
        } 
        return (this.name + "'s" + ' ' + ' health bar is full!')
    }
    
}