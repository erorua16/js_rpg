export enum HeroRace {
    elf = 'elf',
    human = 'human',
    dwarf = 'dwarf'
};