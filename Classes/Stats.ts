export class Stats {
    protected heroHealth:any;
    protected enemyHealth:any;
    protected heroHitStrength:any;
    protected enemyHitStrength:any;
    protected turnInformation:any;
    protected turnNumber:number;
    protected roundNumber:number;
    protected roundUntilDeathNumber:number;
    protected turnDescription:any;
    protected battleInformation:any;
    protected battleTextInformation:any;
    protected battleUntilDeathInformation: any;
    protected battleUntilDeathTextInformation: any;

    constructor(){

        //Individual battle turns
        this.turnNumber = 1;
        this.turnInformation = [];
        this.turnDescription = [];

        //Battle simulation
        this.roundNumber = 0;
        this.battleInformation = [];
        this.battleTextInformation = [];

        //Battle until death
        this.roundUntilDeathNumber = 0;
        this.battleUntilDeathInformation = [];
        this.battleUntilDeathTextInformation = [];
        
    }

////////////////////////////////////////////////////////////////TURN INFORMATION////////////////////////////////////////////////////////////////
    //Turn number
    //To set round number
    addTurn(){
        this.turnNumber += 1
    }
    //To get round number
    getTurnNumber(){
        return this.turnNumber
    }

    //Turn Information
    //To push turn information
    pushToTurnInformation(info:any, info2:any, attackerName:any, damageDealt:number){
        this.turnInformation.push({
            'Turn': this.turnNumber, 
            'Attacker': attackerName,
            'DamageDealt' : damageDealt,
            'hero': info, 
            'enemy': info2
        })
    }
    //To push turn description
    pushToTurnDescription(turnDescription:any){
        this.turnDescription.push(turnDescription)
    }
    //To get turn information
    getTurnInformation(){
        return this.turnInformation
    }
    //To get turn description
    getTurnDescription(){
        return this.turnDescription
    }

////////////////////////////////////////////////////////////////BATTLE SIMULATION INFORMATION////////////////////////////////////////////////////////////////
    
    //Round number
    //To set round number
    addRound(){
        this.roundNumber += 1
    }
    //To get round number
    getRoundNumber():number{
        return this.roundNumber
    }

    //Battle statistics
    //To push to battle information
    pushToBattleInformation(turnInformation:any){
        this.battleInformation.push({
            'Round' : this.roundNumber, 
            'RoundStats': turnInformation
        })
    }
    //To get battle information
    getBattleInformation(){
        return this.battleInformation
    }

    //Battle text
    //To push to battle text information
    pushToBattleTextInformation(turnTextInformation:any){
        this.battleTextInformation.push({
            'Round' : this.roundNumber, 
            'RoundText': turnTextInformation
        })
    }
    //To get battle text information
    getBattleTextInformation(){
        return this.battleTextInformation
    }

////////////////////////////////////////////////////////////////BATTLE UNTIL DEATH INFORMATION////////////////////////////////////////////////////////////////

    
    //Round number
    //To set round number
    addRoundUntilDeath(){
        this.roundUntilDeathNumber += 1
    }
    //To get round number
    getRoundUntilDeathNumber():number{
        return this.roundUntilDeathNumber
    }

    //Battle statistics
    //To push to battle information
    pushToBattleUntilDeathInformation(turnInformation:any){
        this.battleUntilDeathInformation.push({
            'Round' : this.roundUntilDeathNumber, 
            'RoundStats': turnInformation
        })
    }
    //To get battle information
    getBattleUntilDeathInformation(){
        return this.battleUntilDeathInformation
    }

    //Battle text
    //To push to battle text information
    pushToBattleUntilDeathTextInformation(turnTextInformation:any){
        this.battleUntilDeathTextInformation.push({
            'Round' : this.roundUntilDeathNumber, 
            'RoundText': turnTextInformation
        })
    }
    //To get battle text information
    getBattleUntilDeathTextInformation(){
        return this.battleUntilDeathTextInformation
    }

}