export class Character {
  //Variables declarations
  protected name: string;
  protected health: number;
  protected maxHealth:number;
  protected hitStrength: number;
  protected lvl: number;
  protected xp: number;
  protected canDefend: boolean;
  protected dead:boolean;

  //Constructor
  constructor(
  ) {
    this.xp = 0;
    this.canDefend = false;
  }

  ////////////////////////////////////////////////////////////////METHODS FOR CHARACTERS STATISTICS////////////////////////////////////////////////////////////////

  //Name
  //To set a new name
  setName(name: string) {
    this.name = name;
  }
  //To return the name
  getName(): string {
    return this.name;
  }

  //Health
  //To set new health
  setHealth(health: number) {
    this.health = health;
  }
  //To get current health
  getHealth(): number {
    return this.health;
  }
  //To set new max health
  setMaxHealth(health: number) {
    this.maxHealth = health;
  }
  //To get max health
  getMaxHealth(): number {
    return this.maxHealth;
  }

  //Hit strength
  //To set new hitStrength
  setHitStrength(hitStrength: number) {
    this.hitStrength = hitStrength;
  }
  getHitStrength(): number {
    return this.hitStrength;
  }

  //Defence
  //To get if character can defend
  getCanDefend(canDefend: boolean): boolean {
    return this.canDefend;
  }

  //Level
  //To set new level
  setLvl(lvl: number) {
    this.lvl = lvl;
  }
  //To get current level
  getLvl(): number {
    return this.lvl;
  }

  //Experience
  //To set new xp
  setXp(xp: number) {
    this.xp = xp;
  }
  //To get current xp
  getXp(): number {
    return this.xp;
  }

  //To die
  die(){
    return(this.name + ' ' + 'has died! Here are your final stats: level =' + ' ' + this.lvl + ' ' + 'hit strength' + ' ' + this.hitStrength)
  }
}