// Importation of class 'Enemy' for child class 'Assassin'
import { Enemy } from "../enemies/Enemy";

// Class for character 'Assassin'
export class Assassin extends Enemy {

  // new hitStrength after 10% of increase
  private newHitStrength: number;
  // first round
  private firstRound: boolean;

  constructor(maxHealth:number, hitStrength:number) {
    super(maxHealth, hitStrength);
    this.name = 'Assassin'
    this.firstRound = true;
    this.newHitStrength = hitStrength;
  }

  // Attack
  attack() {
    if (this.firstRound == true) {
      this.firstRound = false;
      return this.hitStrength * this.lvl;
    } else {
      return this.powerUp();
    }
  }

  // increasing hitStrength for 10%
  powerUp() {
    this.newHitStrength = (this.newHitStrength * this.lvl) + (this.hitStrength / 10);
    return this.newHitStrength;
  }

}


