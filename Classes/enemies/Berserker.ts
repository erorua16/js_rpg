import { Enemy } from './Enemy';

export class Berserker extends Enemy {

    constructor(maxHealth:number, hitStrength:number){
        super(maxHealth, hitStrength);
        this.name = 'Berserker'
        this.canDefend = true;
    }

    //To attack
    attack(){
        return(this.hitStrength * this.lvl)
    }

    //To defend against attack
    defence(damage: number){
        //Reduce damage by 30%
        return (damage / 1.3);
    }

}