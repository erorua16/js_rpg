import { Enemy } from './Enemy';

export class Golem extends Enemy {

    constructor(maxHealth:number, hitStrength:number){
        super(maxHealth, hitStrength);
        this.name = 'Golem'
        this.canDefend = true;
    }
    
    //To attack
    attack(){
        return(this.hitStrength * this.lvl)
    }
    
    //To defend, a 50% chance to negate all damage
    defence(damage:number){
        const chance:number = 50.00;
            var n = Math.random();
            n = n * 100;
            if(n <= chance){
                return damage = 0;
            }
            else{
                return damage;
            }
    }
    

}
