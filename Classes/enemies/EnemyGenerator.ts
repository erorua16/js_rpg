import { Dragon } from "./Dragon";
import { Griffin } from "./Griffin";
import { Golem } from "./Golem";
import { Werewolf } from "./Werewolf";
import { Berserker } from "./Berserker";
import { Assassin } from "./Assassin";

export class EnemyGenerator {
    private berserker:Berserker
    private griffin:Griffin
    private assassin:Assassin
    private golem:Golem
    private dragon:Dragon
    private werewolf:Werewolf
    private allEnemies:any;
    private generatedEnemy:any;
    
    constructor(){
        this.allEnemies = [];
    }

    //To generate an enemy randomly
    generateEnemy(){
        const random = Math.floor(Math.random() * 6)
        
        switch (random) {
            case 1: 
                this.newBerserker()
                this.generatedEnemy = this.getBerserker()
                return this.generatedEnemy
                break;
            case 2: 
                this.newAssassin()
                this.generatedEnemy = this.getAssassin()
                return this.generatedEnemy
                break;
            case 3: 
                this.newGriffin()
                this.generatedEnemy = this.getGriffin()
                return this.generatedEnemy
                break;
            case 4:
                this.newWerewolf()
                this.generatedEnemy = this.getWerewolf()
                return this.generatedEnemy
                break;
            case 5: 
                this.newGolem()
                this.generatedEnemy = this.getGolem()
                return this.generatedEnemy
                break;
            default:
                this.newDragon()
                this.generatedEnemy = this.getDragon()
                return this.generatedEnemy
                break;
        }
    }
    //To get randomly generated enemy
    getGeneratedEnemy(){
        return this.generatedEnemy
    }

    //To generate all enemies
    generateAllEnemies(){
        this.newBerserker()
        this.newAssassin()
        this.newDragon()
        this.newGolem()
        this.newGriffin()
        this.newWerewolf()
        this.allEnemies.push(
            this.getBerserker(), 
            this.getAssassin(), 
            this.getDragon(), 
            this.getGolem(),
            this.getWerewolf(),
            this.getGriffin()
        )
    }
    //To get all enemies
    getAllEnemies(){
        return this.allEnemies
    }


////////////////////////////////////////////////////////////////CREATING AND GETTING ENEMIES////////////////////////////////////////////////////////////////
    
    //Berserker functions
    //To create new berserker
    // health 60 ~ 85
    // hitStrength 7 ~ 9
    newBerserker(){
        const maxHealth = Math.floor(Math.random() * 25) + 60;
        const hitStrength = Math.floor(Math.random() * 2) + 7;
        this.berserker = new Berserker(maxHealth, hitStrength)
    }
    //To get berserker
    getBerserker(){
        return this.berserker
    }

    //Griffin functions
    //To create new griffin
    // health 75 ~ 90
    // hitStrength 7 ~ 9
    newGriffin(){
        const maxHealth = Math.floor(Math.random() * 15) + 75;
        const hitStrength = Math.floor(Math.random() * 2) + 7;
        this.griffin = new Griffin(maxHealth, hitStrength)
    }
    //To get griffin
    getGriffin(){
        return this.griffin
    }

    //Assassin
    //To create new assassin
    // health 70 ~ 85
    // hitStrength 7 ~ 9
    newAssassin(){
        const maxHealth = Math.floor(Math.random() * 15) + 70;
        const hitStrength = Math.floor(Math.random() * 2) + 7;
        this.assassin = new Assassin(maxHealth, hitStrength)
    }
    //To get assassin
    getAssassin() { 
        return this.assassin 
    }

    //Golem
    //To create new golem
    // health 85 ~ 100
    // hitStrength 4 ~ 7
    newGolem(){
        const maxHealth = Math.floor(Math.random() * 15) + 85;
        const hitStrength = Math.floor(Math.random() * 3) + 4;
        this.golem = new Golem(maxHealth, hitStrength)
    }
    //To get golem
    getGolem() { 
        return this.golem 
    }

    //Werewolf
    //To create new werewolf
    // health 60 ~ 85
    // hitStrength 6 ~ 7.5
    newWerewolf(){
        const maxHealth = Math.floor(Math.random() * 25) + 60;
        const hitStrength = Math.floor(Math.random() * 1.5) + 6;
        this.werewolf = new Werewolf(maxHealth, hitStrength)
    }
    //To get werewolf
    getWerewolf() { 
        return this.werewolf 
    }

    //Dragon
    //To create new dragon
    // health 85 ~ 100
    // hitStrength 8 ~ 10
    newDragon(){
        const maxHealth = Math.floor(Math.random() * 15) + 85;
        const hitStrength = Math.floor(Math.random() * 2) + 8;
        this.dragon = new Dragon(maxHealth, hitStrength)
    }
    //To get dragon
    getDragon() { 
        return this.dragon 
    }
}

// test
// const enemy = new EnemyGenerator
// enemy.generateAllEnemies()
// console.log(enemy.getAllEnemies())