// @TODO FATOU
import { Character } from '../Character';

export class Enemy extends Character {

    protected enemyFly: boolean; 

    constructor(maxHealth:number, hitStrength:number){

        super();
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.hitStrength = hitStrength;
        this.lvl = 1;
        this.enemyFly = false;

    };
}