import { Enemy } from './Enemy';

export class Werewolf extends Enemy {

    constructor(maxHealth:number, hitStrength:number){
        super(maxHealth, hitStrength);
        this.name = 'Werewolf'
        this.canDefend = true;
    }
    
    //To attack
    attack(){
        return(this.hitStrength * this.lvl)
    }
    
    //To defend, reduce damage taken by 50%
    defence(damage:number){
        return damage / 2
    }

}