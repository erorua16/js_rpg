// Importation of class 'Enemy' for child class 'Griffin'
import { Enemy } from "./Enemy";

// Class for character 'Griffin'
export class Griffin extends Enemy {
    
  private flying: boolean;
  private attackTurn:number;

  constructor(maxHealth:number, hitStrength:number){

        super(maxHealth, hitStrength);
        this.name = 'Griffin'
        this.enemyFly = true;
        this.flying = false;
        this.canDefend = true;
        this.attackTurn = 0;

  };

  //Attack in this order (groundAttack -> fly -> skyAttack)
  attack(){
      switch(this.attackTurn){
          case this.attackTurn = 0:
              this.attackTurn += 1
              return this.groundAttack();
              break;
          case this.attackTurn = 1:
              this.attackTurn += 1
              return this.fly()
              break;
          default:
              this.attackTurn = 0
              return this.skyAttack()
              break;
          
      } 
  }

  //To attack when on ground, returns to ground if griffin was flying
  groundAttack(){
      this.flying = false;
      return (this.hitStrength * this.lvl)
  }

  //To get flying status
  getFlying(){
      return this.flying
  }

  //To fly
  fly(){
      this.flying = true;
      return 0
  }

  //To attack when flying , attack gets a 10% bonus
  skyAttack(){
      return (this.hitStrength * 1.1)
  }

  //To defend against attacks
  defence(flying:boolean, damage:number){
      //If griffin is flying, reduce damage by an additional 10%
      if(flying == true){
          return (damage / 1.1)
      }
      return damage;
  }

}
