import { Battle } from "../Battle"
import { Ranking } from "./EnemiesBattleRankingInterface";

//Load lodash library to use clone deep function
const _ = require('lodash');

//Make each enemy fight all the other enemies one by one
//Rank by how many times it died
//Rank by how many total turns fought against all enemies

class BattleEnemiesRanking extends Battle {

    public fighterRankingInformation:Ranking[]
    private fighterTotalDeath:number
    public fighterTurnDescriptions: any
    private totalTurns:number
    private fighterDeath:number

    constructor(){

        super()
        this.fighterRankingInformation = []
        this.fighterTurnDescriptions = []
        this.fighterDeath = 0
        this.fighterTotalDeath = 0
        this.totalTurns = 0

    }

    //Start battles and get the ranking
    startBattleRanking(){

        //Generate all fighters
        this.generateEnemies()
            
            //Loop to go through each fighters and set them against each other
            for(var i = 0; i <= (this.allEnemies.length - 1); i++){
                
            //For last fight
            if( i == (this.allEnemies.length - 1) ){
                fighterOne = this.getOnefighter(this.allEnemies.length - 1)
                fighterTwo = this.getOnefighter(0)
                this.reset()
                
                //Until one fighter dies, loop through the battle
                while(this.battleEnd == false){

                    //Start battle
                    this.fighterBattleRound(fighterOne, fighterTwo)
                    this.fighterBattleRound(fighterTwo, fighterOne)
                    this.endOfBattle(fighterOne, fighterTwo)

                }

                //Push battle statistics
                this.fighterRankingInformation.push({

                    id: i,
                    fighter: fighterOne,
                    totalTurns: this.totalTurns, 
                    totalDeaths: this.fighterTotalDeath

                })

            }
            
            //Other fights
            else {
                
                //Get fighter one
                var fighterOne = this.getOnefighter(i)
                //Get fighter two
                var fighterTwo = this.getOnefighter(i + 1)
                this.reset()

                //Until one fighter dies, loop through the battle
                while(this.battleEnd == false){

                   //Start battle
                   this.fighterBattleRound(fighterOne, fighterTwo)
                   this.fighterBattleRound(fighterTwo, fighterOne)
                   this.endOfBattle(fighterOne, fighterTwo)

               }

               //Push fighter statistics
               this.fighterRankingInformation.push({
                   id: i,
                   fighter: fighterOne, 
                   totalTurns: this.totalTurns, 
                   totalDeaths: this.fighterTotalDeath
                })

            }

        }
        //return the sorted ranking of enemies
        return this.sortedRanking()
    }

    //To get one fighter from generated enemies array
    getOnefighter(fighterIndex:number){

        var fighter = _.cloneDeep(this.allEnemies[fighterIndex])

        return fighter

    }

    //Fighter battle round
    fighterBattleRound(fighterOne:any, fighterTwo:any){

        //COMBAT START
        this.fighterAttack(fighterOne, fighterTwo)
        
        // Add a turn
        this.turnNumber += 1

        //Add total turns
        this.totalTurns += this.turnNumber

        
    }

    //Enemy Attack
    fighterAttack(fighterOne:any, fighterTwo:any){

        //Set original value of the damage fighterOne will deal
        this.damage = fighterOne.attack()

        //Calculate damage according to the fighterOne's defence
        //If the enemy can defend, calculate damage accordingly
        if (fighterTwo.getCanDefend() == true){

            //If the enemy can fly, calculate damage accordingly
            if(fighterTwo.enemyFly == true){
                this.damage = fighterTwo.defence(fighterTwo.getFlying(), this.damage)
            }        //If enemy can't defend, return default damage
            else{
                this.damage = fighterTwo.defence(this.damage)
            }
        }

        //fighterTwo's health to health - damage
        fighterTwo.setHealth(Math.round(fighterTwo.getHealth() - this.damage))

        //Send battle results as text description
        this.fighterTurnDescriptions.push(

            'Turn' +
            ' ' +
            this.getTurnNumber() + 
            ':' + 
            ' ' +
            fighterOne.getName() + 
            ' ' + 
            'dealt' + 
            ' ' + 
            Math.round(this.damage) + 
            ' ' +
            'damage' + 
            ' ' + 
            'to' + 
            ' ' + 
            fighterTwo.getName()

        )
    }

    //Reset statistics
    reset(){

        //Reset total turns of fighter
        this.totalTurns = 0
        //Reset turn count
        this.turnNumber = 1

        //Reset battle over
        this.battleEnd = false
        
        //Reset death
        this.fighterDeath = 0
        //Reset total death
        this.fighterTotalDeath = 0

    }

    //End the battle
    endOfBattle(fighterOne:any, fighterTwo:any){

        //End battle if fighter one dies
        if(fighterOne.getHealth() <= 0){

            //Add to death count
            this.fighterDeath += 1
            //Add total death
            this.fighterTotalDeath += this.fighterDeath
            
            //End battle
            this.battleEnd = true

        }

        //End battle if fighter two dies
        if(fighterTwo.getHealth() <= 0){

            //End battle
            this.battleEnd = true

        }

    }

    //THIS FUNCTION IS OBSOLETE IN THIS CONTEXT (but i'm keeping it in case i might need it at a later time)
    filterRanking(){

        var result:any = [];
        this.fighterRankingInformation.reduce(function(res:any, value:any) {
        if (!res[value.id]) {
            res[value.id] = { id: value.id, fighter: value.fighter, totalTurns: value.totalTurns, totalDeaths: value.totalDeaths };
            result.push(res[value.id])
        }
        res[value.id].totalTurns += value.totalTurns;
        res[value.id].totalDeaths += value.totalDeaths;
        return res;
        }, {});

        return(result)  
    }

    //To sort the fighters by number of death and turn number
    sortedRanking(){

        //Sort by total turns
        this.fighterRankingInformation = this.fighterRankingInformation.sort((a,b) => (a.totalTurns > b.totalTurns) ? 1 : ((b.totalTurns > a.totalTurns) ? -1 : 0))
        
        //Sort by total deaths
        this.fighterRankingInformation.sort((a,b) => (a.totalDeaths > b.totalDeaths)? 1 : ((b.totalDeaths > a.totalDeaths) ? -1 : 0))
        
        return this.fighterRankingInformation
    }

    //To get description of every turns fought
    allturnsDescriptions(){

        return this.fighterTurnDescriptions

    }

}

////////////////////////////////////////////////////////////////****TO TEST****////////////////////////////////////////////////////////////////
// //START ENEMY RANKING SIMULATION

//Create the ranking simulation 
const fight = new BattleEnemiesRanking()

//Get ranking
console.log(fight.startBattleRanking())
//Get the description of every battle turn
// console.log(fight.allturnsDescriptions())

