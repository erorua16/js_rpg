//Interface for ranking statistics

export interface Ranking {

    id: number;
    fighter: any;
    totalTurns: number;
    totalDeaths: number;

}