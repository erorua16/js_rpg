import { Hero } from "../hero/Hero";
import { HeroRace } from '../hero/HeroRace';
import { Stats } from "../Stats"
import { EnemyGenerator } from "../enemies/EnemyGenerator"

//Load lodash library to use clone deep function
const _ = require('lodash');


export class Battle extends Stats{

    protected hero:Hero;
    protected enemy:any;
    protected heroOrEnemyTurn:boolean;
    protected battleRoundEnd:boolean;
    protected damage:number;
    protected allEnemies:any;
    protected battleEnd:boolean;
    protected newEnemy:EnemyGenerator;
    protected enemyIndex:number;
    protected heroDeepCopy:Hero;
    protected enemyDeepCopy:any;
    protected victory:boolean

    constructor(){
        super()
        this.newEnemy = new EnemyGenerator()
        this.allEnemies = [];
        this.heroOrEnemyTurn = true
        this.battleRoundEnd = false
        this.damage = 0
        this.battleEnd = false
        this.victory = false
    }


////////////////////////////////////////////////////////////////GET ENEMIES////////////////////////////////////////////////////////////////

    //To generate all enemies
    generateEnemies(){

        //Generate the enemies
        this.newEnemy.generateAllEnemies()

        //Set the generated enemies as the current enemies for this battle simulation
        this.allEnemies = this.newEnemy.getAllEnemies()

    }


    //To get all generated enemies
    getAllCurrentEnemies(){
        return this.allEnemies
    }


    //To get one enemy of all enemies 
    getOneEnemy(){
        //Get a random index number to pick one of the enemies
        this.enemyIndex = Math.floor(Math.random() * this.allEnemies.length)

        //Set enemy as the chosen enemy.
        this.enemy = this.allEnemies[this.enemyIndex]
    }


////////////////////////////////////////////////////////////////BATTLE ROUND////////////////////////////////////////////////////////////////
    //@TODO Should change hero and enemy to opponnent 1 and opponent 2
    //add them as parameter to make function more flexible and reusue it instead of rewrite it
    
    //To start one battle 
    battleRoundStart(){
        //If battle is not over, keep letting the enemy and hero attack each other
        while(this.battleRoundEnd == false){

        ////////////////HERO TURN////////////////


            if(this.heroOrEnemyTurn == true){
                
                //Add text turn information to the array for turn descriptions
                this.pushToTurnDescription(this.heroTurn())

                //Add statistics of turn information to the array for statistics
                //Deep clone of hero to be able to use the hero copy
                //JSON parse for enemy since enemy is not reused
                this.heroDeepCopy = _.cloneDeep(this.hero)
                this.enemyDeepCopy = JSON.parse(JSON.stringify(this.enemy))

                //Send information to array
                this.pushToTurnInformation(
                    this.heroDeepCopy, 
                    this.enemyDeepCopy, 
                    this.hero.getName(), 
                    Math.round(this.damage)
                    )
                
                //Add a turn to turn counter
                this.addTurn()

                //If enemy is dead, battle ends
                if(this.enemy.getHealth() <= 0){
                    this.enemy.die();
                    this.pushToTurnDescription(this.heroWon() + ' ' + this.hero.gainXP())
                }

            }


        ////////////////ENEMY TURN////////////////


            else {

                //Add text turn information to the array for turn descriptions
                this.pushToTurnDescription(this.enemyTurn())

                //Add statistics of turn information to the array for statistics
                //Deep clone of hero to be able to use the hero copy
                //JSON parse for enemy since enemy is not reused
                this.heroDeepCopy = _.cloneDeep(this.hero)
                this.enemyDeepCopy = {...this.enemy}

                //Send information to array
                this.pushToTurnInformation(
                    this.heroDeepCopy, 
                    this.enemyDeepCopy, 
                    this.enemy.getName(), 
                    Math.round(this.damage)
                    )

                //Add a turn to turn counter
                this.addTurn()

                //If hero is dead, battle ends
                if(this.hero.getHealth() <= 0){
                    this.hero.die();
                    this.pushToTurnDescription(this.heroLost())
                }

            }
        //End of while
        }

        //Clone final hero
        this.heroDeepCopy = _.cloneDeep(this.hero)
    }


////////////////////////////////////////////////////////////////HERO TURN////////////////////////////////////////////////////////////////
    

    //To start hero turn
    heroTurn(){
        //Set so next turn is enemy
        this.heroOrEnemyTurn = false

        //If enemy is dead, finish battle
        if(this.enemy.getHealth() <= 0){

            //kill enemy
            this.enemy.die();
            return (this.attackEnemy() + '.' + ' ' + this.heroWon() + ' ' + this.hero.gainXP())

        }

        //Attack the enemy
        return this.attackEnemy()

    }


    //Hero attacks enemy
    attackEnemy(){
        //Set original value of the damage hero will deal
        this.damage = (this.hero.attack(this.hero.getRace(), this.enemy.enemyFly))

        //If the enemy can defend, calculate damage accordingly
        if (this.enemy.getCanDefend() == true){
            //If the enemy can fly, calculate damage accordingly
            if(this.enemy.enemyFly == true){
                this.damage = this.enemy.defence(this.enemy.getFlying(), this.damage)
            }
            
        //If enemy can't defend, return default damage
        else{
            this.damage = this.enemy.defence(this.damage)
        }

        }

        //Set enemy health to  health - damage
        this.enemy.setHealth(Math.round(this.enemy.getHealth() - this.damage))

        //Return battle results as text description
        return(
            'Turn' +
            ' ' +
            this.getTurnNumber() + 
            ':' + 
            ' ' +
            this.hero.getName() + 
            ' ' + 
            'dealt' + 
            ' ' 
            + 
            Math.round(this.damage) + 
            ' ' + 
            'damage' +
            ' ' +
            'to' +
            ' ' + 
            this.enemy.name)


    }


////////////////////////////////////////////////////////////////ENEMY TURN////////////////////////////////////////////////////////////////
    

    //To start enemy turn
    enemyTurn(){
        //Set so next turn is the hero's turn
        this.heroOrEnemyTurn = true

        //If hero is dead, end battle
        if(this.hero.getHealth() <= 0){

            //Kill hero
            this.hero.die();
            return this.attackHero()
        }
        
        //If hero isn't dead, attack hero
        else {
            return this.attackHero()
        }

    }


    //Enemy Attack
    attackHero(){
        //Set original value of the damage hero will deal
        this.damage = this.enemy.attack()

        //Calculate damage according to the hero's defence
        this.damage = this.hero.defence(this.hero.getRace(), this.damage)

        //Set hero's health to  health - damage
        this.hero.setHealth(Math.round(this.hero.getHealth() - this.damage))

        //Return battle results as text description
        return(
            'Turn' +
            ' ' +
            this.getTurnNumber() + 
            ':' + 
            ' ' +
            this.enemy.getName() + 
            ' ' + 
            'dealt' + 
            ' ' + 
            Math.round(this.damage) + 
            ' ' +
            'damage' + 
            ' ' + 
            'to' + 
            ' ' + 
            this.hero.getName())


    }


////////////////////////////////////////////////////////////////BATTLE END////////////////////////////////////////////////////////////////
   

    //If hero has won
    heroWon(){
        //End the battle
        this.battleRoundEnd = true 

        //Return victory text, and restore the health of the hero.
        return (
            this.hero.getName() + 
            ' ' + 
            'has won!' + 
            ' ' + 
            this.hero.restoreHealth(this.enemy.getMaxHealth()) + 
            '.')
    }


    //If hero has lost
    heroLost(){
        //End battle round
        this.battleRoundEnd = true
        //End battle simulation
        this.battleEnd = true

        return (
            this.hero.getName() + 
            ' ' + 
            'has lost...')
    }

    setHero(hero:Hero){
        this.hero = hero
    }

}

////////////////////////////////////////////////////////////////****TO TEST****////////////////////////////////////////////////////////////////
// //CREATE A HERO AND BATTLE
// //Input name and race (human, elf, or dwarf)
// //Health is set to 110 for best results
// var hero = new Hero('INPUTNAME', HeroRace.INPUTRACE, 110)
// //Create an new battle simulation
// const battler = new Battle()
// battler.setHero(hero)

// // RUN BATTLE
// //Generate enemies
// battler.generateEnemies()
// //Get one generated enemy
// battler.getOneEnemy()
// //Start battle
// battler.battleRoundStart()

// // //TO SEE BATTLE AS TEXT DESCRIPTION RUN:
// console.log(battler.getTurnDescription())

// //TO SEE BATTLE STATS DETAILS RUN:
// console.log(battler.getTurnInformation())
