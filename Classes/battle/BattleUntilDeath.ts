import { BattleSimulation } from "./BattleSimulation"
import { Hero } from "../hero/Hero";
import { HeroRace } from '../hero/HeroRace';
import { EnemyGenerator } from "../enemies/EnemyGenerator"


class BattleUntilDeath extends BattleSimulation {

    makeEnemy:EnemyGenerator;


    constructor(){
        super()
        this.makeEnemy = new EnemyGenerator()
    }


    //To start battle simulation
    battleUntilDeathStart(){

        //As long as the hero isn't dead keep starting battles
        while(this.hero.getHealth() > 0){


            //BATTLE ROUND PREPARATION
            //Generate enemy
            this.enemy = this.makeEnemy.generateEnemy()
            //Start battle against the generated enemy
            this.battleRoundStart()


            //PUSH INFORMATION
            //Add round number
            this.addRoundUntilDeath()
            //Add information of battle to array with all the battles
            this.pushToBattleUntilDeathInformation(this.getTurnInformation())
            //Add text information of batte to array with all the battles
            this.pushToBattleUntilDeathTextInformation((this.getTurnDescription()))
            

            //RESET FOR NEXT ROUND
            //Reset hero and the statistic information
            this.resetBattleRound()


            //End of while
        }
        

        return (
            
            this.hero.getName() + 
            ' ' +
            "lost against" + 
            ' ' + 
            this.enemy.getName() + 
            '.' + 
            ' ' +
            this.hero.getName() +
            ' ' +
            'survived against' +
            ' ' +
            this.roundUntilDeathNumber +
            ' ' +
            'enemies' +
            '.' +
            ' ' +
            'Final level is' +
            ' ' +
            this.hero.getLvl() +
            '.' +
            ' ' + 
            "Try again?"

        )
    }

}


////////////////////////////////////////////////////////////////****TO TEST****////////////////////////////////////////////////////////////////
// //CREATE A HERO AND BATTLE
// //Input name and race (human, elf, or dwarf)
// //Health is set to 1500 for best results
// var hero = new Hero('INPUTNAME', HeroRace.INPUTRACE, 1500)
// //Create an new battle simulation
// const battler = new BattleUntilDeath()
// battler.setHero(hero)
// //To display in terminal run the following console log
// console.log(battler.battleUntilDeathStart())


// //STATISTICS AND DESCRIPTION
// //To get text description of every turn
// console.log(battler.getBattleUntilDeathTextInformation())
// //To get statistics of every turn
// // --- this is an array with many objects that have objects nested in it, 
// // it needs to be treated to see each information individually ---
// // console.log(battler.getBattleUntilDeathInformation())