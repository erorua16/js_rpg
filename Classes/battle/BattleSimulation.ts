import { Battle } from "./Battle"
import { Hero } from "../hero/Hero";
import { HeroRace } from '../hero/HeroRace';

//Load lodash library to use clone deep function
const _ = require('lodash');


export class BattleSimulation extends Battle {

////////////////////////////////////////////////////////////////BATTLE SIMULATION START////////////////////////////////////////////////////////////////

    //To start battle simulation
    battleStart(){

        //Generate array of enemies
        this.generateEnemies()

        //As long as the hero isn't dead and all the enemies haven't been killed, keep starting battles
        while(this.battleEnd == false){


            //BATTLE ROUND PREPARATION
            //Get one enemy from generated array
            this.getOneEnemy()
            //Start one battle against the generated enemy
            this.battleRoundStart()


            //PUSH INFORMATION
            //Add round number
            this.addRound()
            //Add information of battle to array with all the battles
            this.pushToBattleInformation(this.getTurnInformation())
            //Add text information of batte to array with all the battles
            this.pushToBattleTextInformation((this.getTurnDescription()))
            

            //RESET FOR NEXT ROUND
            //Reset hero and the statistic information
            this.resetBattleRound()


            //LAST ROUND
            //If there is only one enemy left, launch final battle 
            if (this.allEnemies.length == 1 && this.battleEnd == false){
                
                //BATTLE PREP
                this.getOneEnemy()
                this.battleRoundStart()

                //Add to round number
                this.addRound()
                 //Add information of battle to array with all the battles
                this.pushToBattleInformation(this.getTurnInformation())
                this.pushToBattleTextInformation((this.getTurnDescription()))

                //VICTORY
                //if hero didn't lose
                if(this.battleEnd == false){
                    //Finish battle simulation
                    this.allEnemies = []
                    this.battleEnd = true
                    this.victory = true
                }
            }
        

        //End of while
        }


        //If hero won
        if(this.victory == true){

            return '!!!!!Congratulations!!!!!' + 
            this.hero.getName() + 
            ',' + 
            ' ' + 
            'you beat all the enemies!'

        }

        //If hero lost
        return (
            
            this.hero.getName() + 
            ' ' + 
            "lost against" + 
            ' ' + 
            this.enemy.getName() + 
            '.' + 
            ' ' +  
            "Would you like to try again?"

        )
    }

////////////////////////////////////////////////////////////////RESET BATTLE ROUND////////////////////////////////////////////////////////////////
    

    //To reset battle round for next battle
    resetBattleRound(){


        //Empy information arrays
        this.turnInformation = [];
        this.turnDescription = [];

        //Reset so the enemies can battle again
        this.battleRoundEnd = false;
        this.hero = _.cloneDeep(this.heroDeepCopy)

        //Remove the enemy the hero won against from enemy array
        this.allEnemies.splice(this.enemyIndex, 1)


    }

}


////////////////////////////////////////////////////////////////****TO TEST****////////////////////////////////////////////////////////////////
// //CREATE A HERO AND BATTLE
// //Input name and race (human, elf, or dwarf)
// //Health is set to 600 for best results
// var hero = new Hero('INPUTNAME', HeroRace.INPUTRACE, 600)
// //Create an new battle simulation
// const battler = new BattleSimulation()
// battler.setHero(hero)
// //To display in terminal run the following console log
// console.log(battler.battleStart())


// //STATISTICS AND DESCRIPTION
// //To get text description of every turn
// console.log(battler.getBattleTextInformation())
// //To get statistics of every turn
// // --- this is an array with many objects that have objects nested in it, 
// // it needs to be treated to see each information individually ---
// // console.log(battler.getBattleInformation())