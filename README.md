# Ts_rpg

## Project description
Rpg logic in javascript and html.

## To Start
1. clone repository
2. go inside the project and run `npm i`

## Testing / Further developement
1. To compile typescript for tests, use `npm run build2`, it runs it in watch mode
2. To compile for html `npm run build` 
3. To display in browser use `npm run start` -- It's static --

## Functionalities
For best results it is recommended to not run the battle simulation, battle until death, or individual battle at the same time.
1. To test the battle until death simulation (Hero fights randomly generated enemies until his death):
    * Go to `Classes/battle/BattleSimulation.ts` and scroll down to `//TO TEST//` and uncomment this section.
    * Change `INPUT NAME` with your chosen hero name 
    * Change `INPUT RACE` with your chosen race (in lowercase: dwarf,human,elf)
    * Health is automatically set to 1500 for best results
    * If you wish to see the battle as text description uncomment 
    ```console.log(battler.getBattleUntilDeathTextInformation())```
    * Run in terminal ```node dist/Classes/battle/BattleUntilDeathSimulation.js```
2. To test the battle simulation (Hero fights until he kills all enemies, or until he dies): 
    * Go to `Classes/battle/BattleSimulation.ts` and scroll down to `//TO TEST//` and uncomment this section.
    * Change `INPUT NAME` with your chosen hero name 
    * Change `INPUT RACE` with your chosen race (in lowercase: dwarf,human,elf)
    * Health is automatically set to 600 for best results
    * If you wish to see the battle as text description uncomment ```console.log(battler.getBattleTextInformation())```
    * Run in terminal ```node dist/Classes/battle/BattleSimulation.js```
3. To test the individual battle function (Hero fights against one enemy): 
    * Go to `Classes/battle/Battle.ts` and scroll down to `//TO TEST//` and uncomment this section.
    * Change `INPUT NAME` with your chosen hero name 
    * Change `INPUT RACE` with your chosen race (in lowercase: dwarf,human,elf)
    * Health is automatically set to 110 for best results
    * If you wish to see the battle description uncomment ```console.log(battler.getTurnDescription())```
    * If you wish to see the battle statistics uncomment ```console.log(battler.getTurnInformation())```
    * Run in terminal ```node dist/Classes/battle/Battle.js```
4. To test the ranking of the enemies (all enemies fight against each other and get ranked by the strongest)
    * Go to `Classes/battle/enemyRanking/BattleEnemiesRanking.ts` and scroll down to `//TO TEST//` and uncomment this section.
    * If you wish to see all the turns description uncomment ```console.log(fight.startBattleRanking())```
    * Run in terminal ```node dist/Classes/battle/enemyRanking/BattleEnemiesRanking.js```

## TODO
0. Make character class that groups what proprieties enemies and hero have in common so as to not repeat it
    * @DONE
1. Hero class 
    * proprieties: name, health, hitStrength, lvl, xp, race
    * methods:  getName, setName, getHealth, setHealth, attack, die, getLvl, setLvl, getXp, setXp, getRace, setRace, getHitStrength, setHitStrength
    * @DONE
2. Enemy class
    * proprieties: name, health, hitStrength, lvl, xp
    * methods: getName, setName, getHealth, setHealth, attack, die, getLvl, setLvl, getXp, setXp, getHitStrength, setHitStrength
    * @DONE
3. Extended classes for enemies
    * Dragon @DONE
    * Golem @DONE
    * Assassin @DONE
    * Griffin @DONE
    * Berserker @DONE
    * Werewolf @DONE
4. Battle so hero can fight an enemy  
    * @DONE
5. Battle simulation
    * Make hero fight until his death @DONE
    * show stats of every battle turn in html table @TODO
    * Make the enemies battle against each other and sort by most powerful @DONE
6. Index page with table of battle info @TODO FATOU
